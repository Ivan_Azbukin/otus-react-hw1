﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController
        :ControllerBase
    {
        private readonly List<User> _users = new List<User>()
        {
            new User()
            {
                Login = "login",
                Password = "password"
            }
        };

        [HttpGet]
        public async Task<ActionResult<List<User>>> GetUsersASync()
        {
            return await Task.FromResult(_users);
        }

        [HttpPost("SignIn")]
        public async Task<IActionResult> SignIn(User requestUser)
        {
            var user = _users.FirstOrDefault(item => item.Login == requestUser.Login);
            if (user == null)
            {
                return NotFound();
            }
            if (user.Password == requestUser.Password)
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}