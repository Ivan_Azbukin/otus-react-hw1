import './App.css';
import LoginForm from './Components/LoginForm';

function App() {
  return (
    <div className="App">
      <LoginForm apiUri={process.env.REACT_APP_USERS_API_URL}></LoginForm>
    </div>
  );
}

export default App;
