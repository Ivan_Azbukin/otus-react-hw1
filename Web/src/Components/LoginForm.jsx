import React, { useState } from 'react';
import axios from 'axios';

const LoginForm = (apiUri) =>{

    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");

    return(
        <form className="App">
            <label>Login</label>
            <br/>
            <input placeholder="Login" value={login} onChange={(event) => setLogin(event.target.value)}/>
            <br/>
            <label>Password</label>
            <br/>
            <input placeholder="Password" type='password' value={password} onChange={(event) => setPassword(event.target.value)} />
            <br/>
            <button onClick={() => checkUser(apiUri, login, password)}>
                Sign In
            </button>
        </form>
        );
}

async function checkUser(apiUri, login, password){
    console.log("start");
    const client = axios.create({
        baseURL: `${apiUri.apiUri}`,
        headers: {
            "Content-type": "application/json"
        }
    });

    try{
        const response = await client.post("/Users/SignIn", {
            login,
            password
        });
        if(response.status === 200){
            alert(`hello, ${login}!`);
        }
        else{
            alert("Wrong login or password!");
        }
    }
    catch(error){
        alert("Wrong login or password!");
        console.log(error);
    }
    console.log("stop");
}


export default LoginForm;